package net.craftminecraft.forge.AELights.tileentity;

import appeng.api.WorldCoord;
import appeng.api.config.RedstoneModeInput;
import appeng.api.events.GridTileLoadEvent;
import appeng.api.events.GridTileUnloadEvent;
import appeng.api.me.tiles.IGridMachine;
import appeng.api.me.util.IGridInterface;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;
import net.craftminecraft.forge.AELights.AELights;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet132TileEntityData;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class TileEntityLightBlock extends TileEntity implements IInventory, IGridMachine {

    Boolean powerStatus = false, networkReady = true, isPoweredByBattery = false;
    private int color;
    private double energy = 0;
    private final double maxEnergy = 20.0D;
    private double batteryMaxEnergy = 0;
    private double batteryEnergy = 0;
    private RedstoneModeInput redstoneModeInput = RedstoneModeInput.Ignore;
    private IGridInterface grid;
    private ItemStack[] slots = new ItemStack[3];
    private String customName = StatCollector.translateToLocal("tile.block.AELight");

    public TileEntityLightBlock() {
        super();
    }

    public void setLightColor(int color) {
        this.color = color;
    }

    public int getLightColor() {
        return color;
    }

    @Override
    public void updateEntity() {
        if (grid != null) {
            if (energy < maxEnergy) {
                float energyRequired = (float)maxEnergy - (float)energy;
                if (getGrid().useMEEnergy(energyRequired, "Charge Light"))
                    energy = 20.0D;
            }
        } else {
            if (energy > 0) {
                energy -= 1;
            } else {
                updateRender();
            }
        }

        if(FMLCommonHandler.instance().getEffectiveSide().isClient()) {
            return;
        }

        // See if a battery is installed...
        if (slots[0] != null) {
            ItemStack energyCell = slots[0];
            NBTTagCompound tag = energyCell.getTagCompound();
            batteryMaxEnergy = 200000;

            // Get Battery Power from NBT Data
            if (tag != null) {
                batteryEnergy = tag.getInteger("storedEnergy");
            } else {
                tag = new NBTTagCompound();
                batteryEnergy = 0;
            }

            // Check to see if the battery needs to be charged
            if (!isPoweredByBattery && batteryEnergy < batteryMaxEnergy) {
                if (grid != null) {
                    if (getGrid().useMEEnergy(20, "Charge Battery")) {
                        batteryEnergy += 20.0D;
                        if (batteryEnergy > batteryMaxEnergy)
                            batteryEnergy = batteryMaxEnergy;
                    }
                }
            }

            // On Battery Backup Power
            if (grid == null && energy <= 0) {
                if (batteryEnergy > 0 && isPoweredByBattery) {
                    batteryEnergy = batteryEnergy - AELights.LightBlockPowerDraw;
                }

                if (batteryEnergy > 0 && !isPoweredByBattery) {
                    isPoweredByBattery = true;
                    updateRender();
                }

                if (batteryEnergy <= 0 && isPoweredByBattery) {
                    batteryEnergy = 0;
                    isPoweredByBattery = false;
                    updateRender();
                }
            } else {
                if (isPoweredByBattery) {
                    isPoweredByBattery = false;
                    updateRender();
                }
            }

            // Update NBT Data to Battery
            tag.setInteger("storedEnergy", (int)batteryEnergy);
            energyCell.setTagCompound(tag);

        } else {    // There is no battery in the slot, make sure we remove any battery data...
            batteryMaxEnergy = 0;
            batteryEnergy = 0;

            if (isPoweredByBattery) {
                isPoweredByBattery = false;
                updateRender();
            }
        }
    }

    @Override
    public Packet getDescriptionPacket() {
        NBTTagCompound nbtTag = new NBTTagCompound();
        nbtTag.setBoolean("ready", networkReady);
        nbtTag.setBoolean("powered", powerStatus);
        nbtTag.setBoolean("battery", isPoweredByBattery);
        nbtTag.setDouble("batteryEnergy", batteryEnergy);
        nbtTag.setDouble("batteryMaxEnergy", batteryMaxEnergy);
        this.writeToNBT(nbtTag);
        return new Packet132TileEntityData(this.xCoord, this.yCoord, this.zCoord, 1, nbtTag);
    }

    @Override
    public void onDataPacket(INetworkManager net, Packet132TileEntityData packet) {
        readFromNBT(packet.data);
        networkReady = packet.data.getBoolean("powered");
        powerStatus = packet.data.getBoolean("ready");
        isPoweredByBattery = packet.data.getBoolean("battery");
        batteryEnergy = packet.data.getDouble("batteryEnergy");
        batteryMaxEnergy = packet.data.getDouble("batteryMaxEnergy");
    }

    @Override
    public void validate() {
        super.validate();
        MinecraftForge.EVENT_BUS.post(new GridTileLoadEvent(this, worldObj, getLocation()));
    }

    @Override
    public void invalidate() {
        super.invalidate();
        MinecraftForge.EVENT_BUS.post(new GridTileUnloadEvent(this, worldObj, getLocation()));
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        energy = nbt.getDouble("storedEnergy");

        NBTTagList nbtTagList = nbt.getTagList("Items");
        this.slots = new ItemStack[this.getSizeInventory()];
        if (nbt.hasKey("CustomName")) {
            this.customName = nbt.getString("CustomName");
        }
        for (int i = 0; i < nbtTagList.tagCount(); ++i) {
            NBTTagCompound nbtTagCompound = (NBTTagCompound) nbtTagList.tagAt(i);
            int j = nbtTagCompound.getByte("Slot") & 255;

            if (j >= 0 && j < this.slots.length) {
                this.slots[j] = ItemStack.loadItemStackFromNBT(nbtTagCompound);
            }
        }
    }

    @Override
    public void writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        nbt.setDouble("storedEnergy", energy);

        NBTTagList nbtTagList = new NBTTagList();
        for (int i = 0; i < this.slots.length; ++i) {
            if (this.slots[i] != null) {
                NBTTagCompound nbtTagCompound = new NBTTagCompound();
                nbtTagCompound.setByte("Slot", (byte) i);
                this.slots[i].writeToNBT(nbtTagCompound);
                nbtTagList.appendTag(nbtTagCompound);
            }
            nbt.setTag("Items", nbtTagList);
            if (this.isInvNameLocalized()) {
                nbt.setString("CustomName", this.customName);
            }
        }
    }

    @Override
    public WorldCoord getLocation() {
        return new WorldCoord(this.xCoord, this.yCoord, this.zCoord);
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void setPowerStatus(boolean hasPower) {
        powerStatus = hasPower;

        PacketDispatcher.sendPacketToAllPlayers(getDescriptionPacket());
        worldObj.scheduleBlockUpdate(xCoord, yCoord, zCoord, getBlockType().blockID, 2);
    }

    @Override
    public boolean isPowered() {
        return powerStatus;
    }

    @Override
    public IGridInterface getGrid() {
        return grid;
    }

    @Override
    public void setGrid(IGridInterface gi) {
        grid = gi;

        if (!worldObj.isRemote) {
            grid = gi;
            if (gi == null) {
                setPowerStatus(false);
            }
            PacketDispatcher.sendPacketToAllPlayers(getDescriptionPacket());
        }
    }

    @Override
    public World getWorld() {
        return worldObj;
    }

    public void setNetworkReady(boolean isReady) {
        networkReady = isReady;

        PacketDispatcher.sendPacketToAllPlayers(getDescriptionPacket());
        worldObj.scheduleBlockUpdate(xCoord, yCoord, zCoord, getBlockType().blockID, 2);
    }

    public boolean isMachineActive() {
        if (isPoweredByBattery) {
            return true;
        }
        if (powerStatus && networkReady) {
            return true;
        } else {
            if (energy > 0) {
                return true;
            }
        }
        return false;
    }

    public boolean getOnBatteryPower() {
        return isPoweredByBattery;
    }

    public double getBatteryEnergy() {
        return batteryEnergy;
    }

    public double getBatteryMaxEnergy() {
        return batteryMaxEnergy;
    }

    public boolean getBatteryInstalled() {
        if (batteryMaxEnergy > 0) return true;
        return false;
    }

    public void onTick() {
        updateRender();
        updateEntity();
    }

    @Override
    public float getPowerDrainPerTick() {
        return AELights.LightBlockPowerDraw;
    }

    public double getEnergy() {
        return energy;
    }

    public void updateRender()
    {
        worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
        worldObj.updateAllLightTypes(xCoord, yCoord, zCoord);
    }

    public void onNeighborBlockChange(World world, int i, int j, int k, int l) {
        updateEntity();
        worldObj.scheduleBlockUpdate(xCoord, yCoord, zCoord, getBlockType().blockID, 2);
    }

    @Override
    public ItemStack getStackInSlot(int i) {
        if (slots[i] != null) {
            return slots[i];
        } else {
            return null;
        }
    }

    @Override
    public void setInventorySlotContents(int i, ItemStack itemStack) {
        this.slots[i] = itemStack;

        if (itemStack != null && itemStack.stackSize > this.getInventoryStackLimit()) {
            itemStack.stackSize = this.getInventoryStackLimit();
        }

        this.onInventoryChanged();
    }

    @Override
    public void closeChest() {
        // not needed...
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer entityPlayer) {
        return true;
    }

    @Override
    public String getInvName() {
        return customName;
    }

    @Override
    public int getSizeInventory() {
        return 3;
    }

    @Override
    public boolean isInvNameLocalized() {
        return true;
    }

    @Override
    public int getInventoryStackLimit() {
        return 1;
    }

    @Override
    public boolean isItemValidForSlot(int i, ItemStack itemStack) {
        switch (i) {
            case 0:
                return true;
            case 1:
                return false;
            case 2:
                return false;
        }
        return false;
        //return false;
        // TODO: Make not false, lol...
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int i) {
        if (this.slots[i] != null) {
            ItemStack itemStack = this.slots[i];
            this.slots[i] = null;
            return itemStack;
        } else {
            return null;
        }
    }

    @Override
    public void openChest() {
        // not needed...
    }

    @Override
    public ItemStack decrStackSize(int i, int j) {
        if (this.slots[i] != null) {
            ItemStack itemStack;
            if (this.slots[i].stackSize <= j) {
                itemStack = this.slots[i];
                this.slots[i] = null;
                this.onInventoryChanged();
                return itemStack;
            } else {
                itemStack = this.slots[i].splitStack(j);
                if (this.slots[i].stackSize == 0) {
                    this.slots[i] = null;
                }
                this.onInventoryChanged();
                return itemStack;
            }
        } else {
            return null;
        }
    }

    public void updateGuiTile(String playerName) {
        Player player = (Player) worldObj.getPlayerEntityByName(playerName);

        if (!worldObj.isRemote)
            PacketDispatcher.sendPacketToPlayer(getDescriptionPacket(), player);
    }

    public RedstoneModeInput getRedstoneMode() {
        return redstoneModeInput;
    }

    public void setRedstoneMode(RedstoneModeInput mode) {
        redstoneModeInput = mode;
    }
}
