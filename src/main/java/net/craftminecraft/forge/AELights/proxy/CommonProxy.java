package net.craftminecraft.forge.AELights.proxy;

import appeng.api.Blocks;
import appeng.api.Materials;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.network.IGuiHandler;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.craftminecraft.forge.AELights.BlockEnum;
import net.craftminecraft.forge.AELights.ItemEnum;
import net.craftminecraft.forge.AELights.container.ContainerAELight;
import net.craftminecraft.forge.AELights.gui.GuiAELight;
import net.craftminecraft.forge.AELights.tileentity.TileEntityLightBlock;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import java.util.logging.Level;

public class CommonProxy implements IGuiHandler {
    public void checkForIDMismatches() {
        for (BlockEnum entry : BlockEnum.values()) {
            if (!entry.getBlockClass().isInstance(Block.blocksList[entry.getID()]))
                FMLLog.log(Level.SEVERE, "!IMPORTANT! AE Lights has found ID mismatches! The Block \"" + entry.getStatName() + "\"with the id " + entry.getID() + " has been overridden by another mod!");
        }

        for (ItemEnum entry : ItemEnum.values()) {
            if (!entry.getItemClass().isInstance(Item.itemsList[entry.getID() + 256]))
                FMLLog.log(Level.SEVERE, "!IMPORTANT! AE Lights has found ID mismatches! The Item \"" + entry.getStatName() + "\"with the id " + entry.getID() + " (in config, ingame it'shifted up by 256 by forge) has been overridden by another mod!");
        }
    }

    public void addRecipes() {
        // Materials
        ItemStack itemLuxDust = new ItemStack(ItemEnum.LUXDUST.getItemInstance(), 2, 0);
        GameRegistry.addShapelessRecipe(itemLuxDust, new Object[]
                {Materials.matQuartzDust.copy(), Materials.matQuartzDustNether.copy(), Item.glowstone, Item.redstone});

        // Light Block
        for (int i = 0; i < 16; i++) {
            ItemStack itemLightBlock = new ItemStack(BlockEnum.LIGHTBLOCK.getBlockInstance(), 6, i);
            GameRegistry.addRecipe(itemLightBlock,
                    "glg",
                    "lxl",
                    "glg",
                    'g', Blocks.blkQuartzGlass.copy(),
                    'l', new ItemStack(ItemEnum.LUXDUST.getItemInstance(), 1, 0),
                    'x', new ItemStack(Item.dyePowder, 1, 15 - i)
            );
        }
    }

    public void RegisterTileEntities() {
        GameRegistry.registerTileEntity(TileEntityLightBlock.class, "tileentity.lightblock");
    }

    public void RegisterRenderers() {

    }

    public void RegisterItems() {
        for (ItemEnum current : ItemEnum.values()) {
            try {
                current.setItemInstance(current.getItemClass().getConstructor(int.class).newInstance(current.getID()));
                GameRegistry.registerItem(current.getItemInstance(), current.getItemInstance().getUnlocalizedName(), "aelights");
            } catch (Throwable e) {
                FMLLog.log(Level.SEVERE, "There was en error registering the item");
                FMLLog.log(Level.SEVERE, "Error:");
                FMLLog.log(Level.SEVERE, e.getStackTrace().toString());
            }
        }
    }

    public void RegisterBlocks() {
        for (BlockEnum current : BlockEnum.values()) {
            try {
                current.setBlockInstance(current.getBlockClass().getConstructor(int.class).newInstance(current.getID()));
                GameRegistry.registerBlock(current.getBlockInstance(), current.getItemBlockClass(), current.getBlockInstance().getUnlocalizedName());
            } catch (Throwable e) {
                FMLLog.log(Level.SEVERE, "There was en error registering the block");
                FMLLog.log(Level.SEVERE, "Error:");
                FMLLog.log(Level.SEVERE, e.getStackTrace().toString());
            }
        }
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
        if (tileEntity != null) {
            switch (ID) {
                case 0: // AE Light GUI
                    return new GuiAELight(world, (TileEntityLightBlock) tileEntity, player);
                default:
                    return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
        if (tileEntity != null) {
            switch (ID) {
                case 0: // AE Light GUI
                    return new ContainerAELight(player.inventory, (TileEntityLightBlock) tileEntity);
                default:
                    return false;
            }
        } else {
            return false;
        }
    }

    public String getStringLocalization(String key) {
        return LanguageRegistry.instance().getStringLocalization(key);
    }

    public void loadLocalization(String filename, String locale) {
        LanguageRegistry.instance().loadLocalization(filename, locale, true);
    }
}
