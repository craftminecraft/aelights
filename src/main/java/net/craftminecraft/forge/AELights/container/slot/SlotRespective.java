package net.craftminecraft.forge.AELights.container.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotRespective extends Slot {
    IInventory inventory;

    public SlotRespective(IInventory inventory, int index, int x, int y) {
        super (inventory, index, x, y);
        this.inventory = inventory;
    }

    @Override
    public boolean isItemValid(ItemStack itemStack) {
        return inventory.isItemValidForSlot(this.slotNumber, itemStack);
    }
}
