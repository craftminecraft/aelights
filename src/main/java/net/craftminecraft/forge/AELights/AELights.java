package net.craftminecraft.forge.AELights;

import cpw.mods.fml.common.event.FMLInterModComms;
import net.craftminecraft.forge.AELights.localization.LocalizationHandler;
import net.craftminecraft.forge.AELights.network.AbstractPacket;
import net.craftminecraft.forge.AELights.network.PacketHandler;
import net.craftminecraft.forge.AELights.proxy.CommonProxy;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.Configuration;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraftforge.common.Property;

@Mod(modid = "AELights", name = "AE Lights", dependencies = "after:Waila;required-after:AppliedEnergistics")
@NetworkMod(channels =
        {AbstractPacket.CHANNEL}, clientSideRequired = true, serverSideRequired = true, packetHandler = PacketHandler.class)
public class AELights {
    @Instance("AELights")
    public static AELights instance;
    public static float LightBlockPowerDraw = 2.0F;

    public static CreativeTabs ModTab = new CreativeTabs("tabAELights") {
        public ItemStack getIconItemStack() {
            return new ItemStack(BlockEnum.LIGHTBLOCK.getBlockInstance(), 1, 4);
        }
    };

    public static int renderID;

    @SidedProxy(clientSide = "net.craftminecraft.forge.AELights.proxy.ClientProxy", serverSide = "net.craftminecraft.forge.AELights.proxy.CommonProxy")
    public static CommonProxy proxy;

    public static boolean debug;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        NetworkRegistry.instance().registerGuiHandler(this, proxy);
        instance = this;
        LocalizationHandler.loadLanguages();

        // Config
        Configuration config = new Configuration(event.getSuggestedConfigurationFile());
        config.load();

        // Items
        for (ItemEnum current : ItemEnum.values()) {
            current.setID(config.getItem(current.getIDName() + "_ID", current.getID(), current.getDescription()).getInt());
        }

        // Blocks
        for (BlockEnum current : BlockEnum.values()) {
            current.setID(config.getBlock(current.getIDName() + "_ID", current.getID(), current.getDescription()).getInt());
        }

        // Misc Options
        LightBlockPowerDraw = (float)config.get("Power Draw", "LightBlockPowerDraw", "2", "Amount of power each light will draw, 0 is disabled, number is measured in AE/t").getInt();

        config.save();
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.RegisterItems();
        proxy.RegisterBlocks();
        proxy.RegisterRenderers();
        proxy.RegisterTileEntities();

        proxy.addRecipes();

        LanguageRegistry.instance().addStringLocalization("itemGroup.AE_Lights", "en_US", "AE Lights");

        // WAILA Support
        FMLInterModComms.sendMessage("Waila", "register", "net.craftminecraft.forge.AELights.integration.WAILA.WailaDataProvider.callbackRegister");
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.checkForIDMismatches();
    }
}
