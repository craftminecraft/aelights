package net.craftminecraft.forge.AELights.integration.WAILA;

import cpw.mods.fml.common.FMLLog;
import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import mcp.mobius.waila.api.IWailaDataProvider;
import mcp.mobius.waila.api.IWailaRegistrar;
import net.craftminecraft.forge.AELights.tileentity.TileEntityLightBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.StatCollector;
import java.util.List;
import java.util.logging.Level;

public class WailaDataProvider implements IWailaDataProvider {
    @Override
    public ItemStack getWailaStack(IWailaDataAccessor accessor, IWailaConfigHandler config) {
        return null;
    }

    @Override
    public List<String> getWailaHead(ItemStack itemStack, List<String> currenttip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
        return currenttip;
    }

    @Override
    public List<String> getWailaBody(ItemStack itemStack, List<String> currenttip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
        List<String> list = currenttip;
        Class clazz = accessor.getClass();
        TileEntity tileEntity = accessor.getTileEntity();

        if (tileEntity instanceof TileEntityLightBlock) {
            TileEntityLightBlock te = (TileEntityLightBlock) tileEntity;

            list.add(StatCollector.translateToLocal("tooltip.storedenergy") + ": " + te.getEnergy() + " AE / 20 AE");
            list.add(StatCollector.translateToLocal("tooltip.batteryenergy") + ": " + te.getBatteryEnergy() + " AE / " + te.getBatteryMaxEnergy() + " AE");
        }

        return list;
    }

    @Override
    public List<String> getWailaTail(ItemStack itemStack, List<String> currenttip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
        return currenttip;
    }

    @SuppressWarnings("UnusedDeclaration")
    public static void callbackRegister(IWailaRegistrar registrar) {
        FMLLog.log(Level.INFO, "AE Lights <-> Waila");
        IWailaDataProvider provider = new WailaDataProvider();
        registrar.registerBodyProvider(provider, TileEntityLightBlock.class);
    }
}
