package net.craftminecraft.forge.AELights.localization;

import net.craftminecraft.forge.AELights.AELights;

public class LocalizationHandler {
    public static void loadLanguages() {
        for (final Localization localeFile : Localization.values()) {
            AELights.proxy.loadLocalization(localeFile.filename(), localeFile.locale());
        }
    }
}
