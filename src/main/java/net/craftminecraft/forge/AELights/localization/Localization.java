package net.craftminecraft.forge.AELights.localization;

public enum Localization {
    US("en_US");

    private final String locale;

    Localization(String locale) {
        this.locale = locale;
    }

    public String filename() {
        return String.format("/assets/ae_lights/lang/%s.xml", locale);
    }

    public String locale() {
        return locale;
    }
}
