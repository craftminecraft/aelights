package net.craftminecraft.forge.AELights.blocks;

import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemBlockLightBlock extends ItemBlock {
    public ItemBlockLightBlock(int par1) {
        super (par1);
        setHasSubtypes(true);
    }

    public String getUnlocalizedName(ItemStack itemStack) {
        String name = "";
        name = "aelights.lightblock.color" + itemStack.getItemDamage();

        return name;
    }

    public int getMetadata(int par1) {
        return par1;
    }
}
