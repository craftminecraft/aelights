package net.craftminecraft.forge.AELights.container;

import appeng.api.Blocks;
import cpw.mods.fml.common.FMLLog;
import net.craftminecraft.forge.AELights.tileentity.TileEntityLightBlock;
import net.craftminecraft.forge.AELights.container.slot.SlotRespective;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import java.util.logging.Level;

public class ContainerAELight extends Container {
    IInventory tileEntity;

    public ContainerAELight(InventoryPlayer inventory, TileEntityLightBlock tileEntity) {
        this.tileEntity = tileEntity;

        // Energy storage cell
        addSlotToContainer(new Slot(tileEntity, 0, 116, 17) {
            public boolean isItemValid(ItemStack itemStack) {
                return itemStack.isItemEqual(Blocks.blkEnergyCell.copy());
            }
        });

        // TODO: Add anti-mob device
        addSlotToContainer(new Slot(tileEntity, 1, 116, 35) {
            public boolean isItemValid(ItemStack itemStack) {
                return false;
            }
        });

        // TODO: Add controller card
        addSlotToContainer(new Slot(tileEntity, 2, 116, 53) {
            public boolean isItemValid(ItemStack itemStack) {
                return false;
            }
        });

        bindPlayerInventory(inventory);
    }

    protected void bindPlayerInventory(InventoryPlayer inventoryPlayer) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 9; j++) {
                addSlotToContainer(new Slot(inventoryPlayer, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (int i = 0; i < 9; i++) {
            addSlotToContainer(new Slot(inventoryPlayer, i, 8 + i * 18, 142));
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer entityPlayer) {
        return true;
    }

    @Override
    public void onContainerClosed(EntityPlayer entityPlayer) {
        super.onContainerClosed(entityPlayer);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer p, int i) {
        ItemStack itemStack = null;



        return itemStack;
    }
}
