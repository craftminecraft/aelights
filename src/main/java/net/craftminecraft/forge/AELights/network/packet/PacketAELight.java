package net.craftminecraft.forge.AELights.network.packet;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.relauncher.Side;
import net.craftminecraft.forge.AELights.network.AbstractPacket;
import net.craftminecraft.forge.AELights.tileentity.TileEntityLightBlock;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;

import java.net.ProtocolException;

public class PacketAELight extends AbstractPacket {
    World world;
    int x, y, z;
    String playerName;

    public PacketAELight(World world, int x, int y, int z, String playerName) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
        this.playerName = playerName;
    }

    public PacketAELight() {

    }

    @Override
    public void write(ByteArrayDataOutput out) {
        out.writeInt(world.provider.dimensionId);
        out.writeInt(x);
        out.writeInt(y);
        out.writeInt(z);
        out.writeUTF(playerName);
    }

    @Override
    public void read(ByteArrayDataInput in) throws ProtocolException {
        world = DimensionManager.getWorld(in.readInt());
        x = in.readInt();
        y = in.readInt();
        z = in.readInt();
        playerName = in.readUTF();
    }

    @Override
    public void execute(EntityPlayer player, Side side) throws ProtocolException {
        if (side.isServer()) {
            TileEntityLightBlock tileEntity = (TileEntityLightBlock) world.getBlockTileEntity(x, y, z);
            tileEntity.updateGuiTile(playerName);
        }
    }
}
