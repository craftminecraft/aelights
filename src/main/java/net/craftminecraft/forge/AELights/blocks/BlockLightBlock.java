package net.craftminecraft.forge.AELights.blocks;

import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.craftminecraft.forge.AELights.AELights;
import net.craftminecraft.forge.AELights.tileentity.TileEntityLightBlock;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import java.util.List;
import java.util.Random;
import java.util.logging.Level;

public class BlockLightBlock extends Block {

    public Icon[] onIcons = new Icon[16];
    public Icon[] offIcons = new Icon[16];

    public BlockLightBlock(int id) {
        super (id, Material.glass);
        this.setCreativeTab(AELights.ModTab);
        this.setUnlocalizedName("block.lightblock");
        this.setHardness(0.5F);
        this.setResistance(0.5F);
        this.setLightValue(0.0F);
    }

    @Override
    public Icon getIcon(int side, int meta) {
        return offIcons[meta];
    }

    public static <T> T getTileEntity(IBlockAccess access, int x, int y, int z, Class<T> clazz)
    {
        TileEntity te = access.getBlockTileEntity(x, y, z);
        return !clazz.isInstance(te) ? null : (T) te;
    }

    @Override
    public Icon getBlockTexture(IBlockAccess world, int x, int y, int z, int side) {
        TileEntityLightBlock tile = getTileEntity(world, x, y, z, TileEntityLightBlock.class);

        if (tile != null) {
            if (tile.isMachineActive())
                return onIcons[tile.getLightColor()];
            else
                return offIcons[tile.getLightColor()];
        }

        return super.getBlockTexture(world, x, y, z, side);
    }

    @Override
    public int getLightValue(IBlockAccess world, int x, int y, int z) {
        if (world.getBlockTileEntity(x, y, z) == null) {
            return 0;
        }

        TileEntityLightBlock tile = getTileEntity(world, x, y, z, TileEntityLightBlock.class);

        if (tile == null) {
            return 0;
        }

        if (tile.isMachineActive()) {
            if (tile.getOnBatteryPower()) {
                return 13;
            } else
                return 15;
        } else
            return 0;
    }

    @Override
    public void updateTick(World world, int x, int y, int z, Random par5Random)
    {
        TileEntityLightBlock tile = getTileEntity(world, x, y, z, TileEntityLightBlock.class);
        if (tile != null) {
            tile.onTick();
        }
    }

    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister iconRegister) {
        for (int i = 0; i < 16; i++) {
            onIcons[i] = iconRegister.registerIcon("ae_lights:light.on." + i);
            offIcons[i] = iconRegister.registerIcon("ae_lights:light.off." + i);
        }
    }

    @Override
    public TileEntity createTileEntity(World world, int meta) {
        FMLLog.log(Level.INFO, "Creating new Tile Entity");
        TileEntityLightBlock t = new TileEntityLightBlock();
        t.setLightColor(meta);
        return t;
    }

    @Override
    public int damageDropped(int par1) {
        return par1;
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int size, float offsetX, float offsetY, float offsetZ) {
        if (!world.isRemote) {
            if (world.getBlockTileEntity(x, y, z) == null || player.isSneaking()) {
                return false;
            }

            player.openGui(AELights.instance, 0, world, x, y, z);
        }
        return true;
    }

    @Override
    public void onNeighborBlockChange(World world, int x, int y, int z, int neighbourID) {
        if (!world.isRemote) {
            ((TileEntityLightBlock) world.getBlockTileEntity(x, y, z)).onNeighborBlockChange(world, x, y, z, neighbourID);
        }
    }

    @Override
    public void onBlockAdded(World world, int x, int y, int z) {
        onNeighborBlockChange(world, x, y, z, 0);
    }

    @Override
    public boolean renderAsNormalBlock() {
        return true;
    }

    @Override
    public int getRenderType() {
        return 0;
    }

    @Override
    public boolean isOpaqueCube() {
        return true;
    }

    @Override
    public boolean isBlockNormalCube(World world, int x, int y, int z) {
        return true;
    }

    @Override
    public void getSubBlocks(int id, CreativeTabs tab, List list) {
        for (int i = 0; i < 16; i++) {
            list.add(new ItemStack(id, 1, i));
        }
    }

    @Override
    public boolean canCreatureSpawn(EnumCreatureType type, World world, int x, int y, int z) {
        return false;
    }

    @Override
    public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side) {
        return false;
    }

    @Override
    public boolean hasTileEntity(int meta) {
        return true;
    }

    @Override
    public void breakBlock(World world, int x, int y, int z, int par5, int par6) {
        dropItems(world, x, y, z);
        super.breakBlock(world, x, y, z, par5, par6);
    }

    private void dropItems(World world, int x, int y, int z) {
        Random rand = new Random();

        TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
        if (!(tileEntity instanceof IInventory)) {
            return;
        }

        IInventory inventory = (IInventory) tileEntity;

        for (int i = 0; i < inventory.getSizeInventory(); i++) {
            ItemStack item = inventory.getStackInSlot(i);

            if (item != null && item.stackSize > 0) {
                float rx = rand.nextFloat() * 0.8F + 0.1F;
                float ry = rand.nextFloat() * 0.8F + 0.1F;
                float rz = rand.nextFloat() * 0.8F + 0.1F;

                EntityItem entityItem = new EntityItem(world, x + rx, y + ry, z + rz, new ItemStack(item.itemID, item.stackSize, item.getItemDamage()));

                if (item.hasTagCompound()) {
                    entityItem.getEntityItem().setTagCompound((NBTTagCompound) item.getTagCompound().copy());
                }

                float factor = 0.05F;
                entityItem.motionX = rand.nextGaussian() * factor;
                entityItem.motionY = rand.nextGaussian() * factor + 0.2F;
                entityItem.motionZ = rand.nextGaussian() * factor;
                world.spawnEntityInWorld(entityItem);
                item.stackSize = 0;
            }
        }
    }
}
