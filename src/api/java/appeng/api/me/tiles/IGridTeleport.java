package appeng.api.me.tiles;

import appeng.api.DimentionalCoord;

/**
 * Create a connection to a normally not connected tileentity
 */
public interface IGridTeleport
{
	DimentionalCoord[] findRemoteSide();
}
