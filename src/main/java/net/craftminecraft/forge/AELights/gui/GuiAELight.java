package net.craftminecraft.forge.AELights.gui;

import cpw.mods.fml.common.network.PacketDispatcher;
import net.craftminecraft.forge.AELights.BlockEnum;
import net.craftminecraft.forge.AELights.container.ContainerAELight;
import net.craftminecraft.forge.AELights.gui.widget.WidgetRedstoneModes;
import net.craftminecraft.forge.AELights.network.packet.PacketAELight;
import net.craftminecraft.forge.AELights.tileentity.TileEntityLightBlock;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import org.lwjgl.opengl.GL11;

public class GuiAELight extends GuiContainer {
    TileEntityLightBlock tileEntity;
    World world;
    EntityPlayer player;
    public static final int xSize = 176;
    public static final int ySize = 166;
    private double batteryEnergy;
    private double batteryEnergyMax;
    private ResourceLocation guiTexture = new ResourceLocation("ae_lights", "textures/gui/melightgui.png");

    public GuiAELight(World world, TileEntityLightBlock tileEntity, EntityPlayer player) {
        super(new ContainerAELight(player.inventory, tileEntity));
        this.world = world;
        this.tileEntity = tileEntity;
        this.player = player;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {
        drawDefaultBackground();
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        Minecraft.getMinecraft().renderEngine.bindTexture(guiTexture);
        int posX = (width - xSize) / 2;
        int posY = (height - ySize) / 2;
        drawTexturedModalRect(posX, posY, 0, 0, xSize, ySize);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int i, int j) {
        this.fontRenderer.drawString(StatCollector.translateToLocal("gui.aelightwindow"), 5, 5, 0x000000);
        this.fontRenderer.drawString(StatCollector.translateToLocal("gui.aelightwindow.batterystatus") + ":", 5, 20, 0x000000);
        this.fontRenderer.drawString(batteryEnergy + " AE of", 5, 30, 0x000000);
        this.fontRenderer.drawString(batteryEnergyMax + " AE", 5, 40, 0x000000);

        if (tileEntity != null) {
            WidgetRedstoneModes redstoneSwitch = (WidgetRedstoneModes) buttonList.get(0);
            redstoneSwitch.setRedstoneMode(tileEntity.getRedstoneMode());
        }
    }

    public void updateScreen() {
        PacketDispatcher.sendPacketToServer(new PacketAELight(world, tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord, player.username).makePacket());

        if (world.getBlockTileEntity(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord) instanceof TileEntityLightBlock) {
            TileEntityLightBlock tileEntityLightBlock = (TileEntityLightBlock) world.getBlockTileEntity(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord);
            batteryEnergy = tileEntityLightBlock.getBatteryEnergy();
            batteryEnergyMax = tileEntityLightBlock.getBatteryMaxEnergy();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void initGui() {
        super.initGui();
        buttonList.add(new WidgetRedstoneModes(0, guiLeft + 140, guiTop + 17, 16, 16, tileEntity.getRedstoneMode()));
    }

    public void actionPerformed(GuiButton button) {
        switch (button.id) {
            case 0:
                PacketDispatcher.sendPacketToServer(new PacketAELight(world, tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord, player.username).makePacket());
                break;
            default:
        }
    }

    public int getRowLength() {
        return 1;
    }
}
