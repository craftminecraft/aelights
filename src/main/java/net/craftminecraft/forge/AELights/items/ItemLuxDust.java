package net.craftminecraft.forge.AELights.items;

import net.craftminecraft.forge.AELights.AELights;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.util.StatCollector;

public class ItemLuxDust extends Item {

    private Icon icon;

    public ItemLuxDust (int id) {
        super(id);
        this.setMaxStackSize(64);
        this.setMaxDamage(0);
        this.setHasSubtypes(false);
        this.setCreativeTab(AELights.ModTab);
    }

    public Icon getIconFromDamage(int dmg) {
        return this.icon;
    }

    @Override
    public void registerIcons(IconRegister IconRegister) {
        this.icon = IconRegister.registerIcon("ae_lights:item.luxdust");
    }

    @Override
    public String getUnlocalizedName(ItemStack itemStack) {
        return "item.luxdust";
    }

    @Override
    public String getItemDisplayName(ItemStack itemStack) {
        return StatCollector.translateToLocal(getUnlocalizedName(itemStack));
    }
}
